# IaC and Orchestration for the AP Trust Environment and SaaS Products

This repo consists of all the code to configure, build, deploy, test and maintain the infrastructure that supports, hosts, and manages the Aacademic Preservation Trust Environment and it's SaaS architecture. Code will consist of the AWS CloudFormation, Ansible playbooks, containers, CI/CD code, and automated tools to achieve a GitOps framework using CI/CD processes. The mission is to provide a reliable, flexible, and secure 'DevSecOps' orchestration service that supports a resilent enviroment with maximum velocity. 

The primary infrastructure as code (IaC) tools found in this repo are:
- ansible playbooks
- AWS CloudFormation
- aws credentials (encrypted)
- system credentials (yaml) (encrypted)
- system variables (yaml)
- inventory
- j2 templates
- ansible roles
- ansible.cfg file
- docker files
- docker service files (docker-compose for local testing )
- orchestration and CI/CD files (yaml)
- terraform test templates
- aws cli

# How to use this repo and deploy reliably. 

Running the ansible playbooks and additional code successfully in this repo **requires** using consistent settings and configurations in combination with the ansible controller. This section provides guidance on how these components are applied, and the instructions to configure your system to do so. Five components accomplish this process:
- The ansible controller (MacBook Pro)
- ansible.cfg file
- ssh config file
- hosts file (inventory)
- aws cli 

   1. **Ansible.cfg file:** 
   This file works from the root directory of the repo, defining default settings used by the ansible-playbooks. Examples are the location for roles, inventory, password , encyption files, and ssh behaviours. The playbooks are written with the assumption that the ansible.cfg exists in the rpo, and has been configured. If there are other config files, such as the .ansible.cfg file designed to be at the root of the contoller user directory, that file should be removed as it will over ride the repo ansible.cfg file.
   2. **ssh config file:**
   This is the file found on linux, unix, and mac home directories under .ssh, to configure the ssh settings for hosts or groups of hosts. This file is not specific to ansible, but is one of the primary linux native tools that ansible uses to provide configuration management to the infrastructure. Ansible uses ssh to login to the managaed servers and make changes. Ansible will use the ssh foncig file to login to the hosts that consisttue the inventory.
   3. **hosts file:**
   The file consisting of the inventory of hosts or host groups being managed by ansible. Additional ansible specific configurations including environment variables, python versions and ports have been added to the hosts file, for the remote hosts, and the controller (locahost). The localhost configuration is critical for AWS resources to be configured. 
   4. **ansible controller:**
   Currently the ansible controller(s) are the MacBook workstations used by the AP Trust DevOps Administrators. For successful playbook running, the ansible and python environments must be kept conistent across all MacBooks. Fortunately, this can be achieved using the HomeBrew package manager -[HomeBrew](https://homebrew.sh), and ansible itself.To install and configure both, follow the instructions below:
   - For Catalina and Big Sur:
   `sudo /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
     Brew should begin to install and configure all necessary dependendencies. This might take between 5 and 10 minutes. You will know it is complete when it suggests your next steps. Next steps:
   `brew install ansible`
     Brew will begin to install the correct python libraries as well as ansible, and create links accordingly. By default, this will be a python3 library. When complete, entering `ansible --version` should show the ansible version as **`2.10.7`** and the python version as **`3.9.2`**, both running from the `/usr/local/bin` directory. 
   5. **aws-cli**
   Required for deploying AP Trust AWS infrasructure through the aws api. Ansible modules can be configured to use different AWS profiles and credentials, but will default to local settings, and are run from localhost. Currently ansible is not used to install the awscli locally, so it is recommmended to either use homebrew `brew install awscli` or `pip3 install awscli`. Documentation on the aws cli can be found [here](https://docs.aws.amazon.com/comprehend/latest/dg/setup-awscli.html)  

More details can be found at: https://trello.com/c/CF0oB3VR 







[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

Let's test the fun.
