#!/bin/sh

# Define the rclone config file path
RCLONE_CONFIG_FILE=/config/rclone/rclone.conf

# Ensure the config file exists
if [ ! -f "$RCLONE_CONFIG_FILE" ]; then
    echo "rclone.conf not found! Exiting..."
    exit 1
fi

# Replace placeholders with environment variables
sed -i "s|\$wasabi_access_key|$WASABI_ACCESS_KEY|g" "$RCLONE_CONFIG_FILE"
sed -i "s|\$wasabi_secret_key|$WASABI_SECRET_KEY|g" "$RCLONE_CONFIG_FILE"
sed -i "s|\$aws_access_key|$AWS_ACCESS_KEY|g" "$RCLONE_CONFIG_FILE"
sed -i "s|\$aws_secret_key|$AWS_SECRET_KEY|g" "$RCLONE_CONFIG_FILE"


#Copy logs from Wasabi to AWS s3 buckets from the last 24 hours. 
rclone copy --max-age 24h Wasabi-logs:staging.s3.logs AWS-buckets:s3.wasabi.staging.logs
rclone copy --max-age 24h Wasabi-logs:demo.s3.logs AWS-buckets:s3.wasabi.demo.logs
rclone copy --max-age 24h Wasabi-logs:prod.s3.logs AWS-buckets:s3.wasabi.prod.logs


# Run rclone check between the two sources and determine if they match 
DIFF_STAGING=$(rclone check Wasabi-logs:staging.s3.logs AWS-buckets:s3.wasabi.staging.logs 2>&1)
DIFF_DEMO=$(rclone check Wasabi-logs:demo.s3.logs AWS-buckets:s3.wasabi.demo.logs 2>&1)
DIFF_PRDO=$(rclone check Wasabi-logs:prod.s3.logs AWS-buckets:s3.wasabi.prod.logs 2>&1)

# If there are differences for staging, run rclone sync
if echo "$DIFF_STAGING" | grep -q "0 differences found"; then
    echo "No differences found. Skipping sync."
else
    echo "Differences found. Running sync..."
    rclone sync Wasabi-logs:staging.s3.logs AWS-buckets:s3.wasabi.staging.logs
fi

# If there are differences for demo, run rclone sync
if echo "$DIFF_OUTPUT" | grep -q "0 differences found"; then
    echo "No differences found. Skipping sync."
else
    echo "Differences found. Running sync..."
    rclone sync Wasabi-logs:demo.s3.logs AWS-buckets:s3.wasabi.demo.logs
fi

# If there are differences for prod, run rclone sync
if echo "$DIFF_OUTPUT" | grep -q "0 differences found"; then
    echo "No differences found. Skipping sync."
else
    echo "Differences found. Running sync..."
    rclone sync Wasabi-logs:prod.s3.logs AWS-buckets:s3.wasabi.prod.logs
fi


# Execute rclone with passed arguments
#exec "$@"
