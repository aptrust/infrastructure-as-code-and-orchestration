#!/usr/bin/env ruby

# These are the WorkItem ids we want to requeue
ids = [1,2,3]

# Requeue them one by one.
# Use your own credentials in the API-User and API-Key headers
# Note the post parameter stage=Requested.

# Set this to the appropriate stage.
ids.each do |id|
    puts id
    result = `curl -s -H 'X-Pharos-API-User: your.email@aptrust.org' -H 'X-Pharos-API-Key: your.api.key' -X PUT -d 'stage=Receive'  https://repo.aptrust.org/admin-api/v3/items/requeue/#{id}`
    puts result
    sleep(0.1)
end

