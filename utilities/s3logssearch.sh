#!/bin/sh
# Vars
# $bucket- bucket to search
# $date - to search
# $item - the text words or file to search
# $logsdir - local directory to copy to and search
# $s3athena - bucket to copy to for searching
# $results - results file

$bucket=aptrust.receiving.colorado.edu
$date=2023-08-01
$logsdir=s3logs


aws s3 ls s3://aptrust.s3.logs/$bucket --recursive | awk '{print $4}' > awslogs.txt
echo "A list of the logs for $bucket have been downloaded"


cat awslogs.txt | grep '^$date' > logstosearch.txt
echo "A list of the logs to download and search on $date has been created"

#download the logs
file=$(cat logstosearch.txt)
for line in $file
do 
aws s3 cp s3://aptrust.s3.logs/"$line" $logsdir/ > /dev/null

done

echo " Logs have been downloaded, preparing to search"







