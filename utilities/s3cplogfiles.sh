#!/bin/bash
file=$(cat colorado-07-29.txt)
for line in $file
do
aws s3 cp s3://aptrust.s3.logs/"$line" s3://aptrust.athena.data2/aptrust.receiving.colorado.edu/y=2023/m=07/d=29/ > /dev/null
done
echo "You have copied over all of the logs from file colorado-07-29.txt."

