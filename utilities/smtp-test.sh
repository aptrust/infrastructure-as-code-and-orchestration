#!/bin/bash

# Set the SMTP server name and port number
SMTP_SERVER="email-smtp.us-east-1.amazonaws.com"
SMTP_PORT="587"

# Set the sender email address and AWS SES username and password
SENDER_EMAIL="flavia.ruffner@aptrust.org"
SES_USERNAME="AKIA6QO2K7LI2EMITGEC"
SES_PASSWORD="BGivAzYTHoZrJ2MJMVodvBrA+PnbfUuWLL+Rc0RY4mmD"

# Set the recipient email address
RECIPIENT_EMAIL="ops@aptrust.org"

# Test SMTP credentials using openssl and telnet
echo "Testing SMTP credentials..."
openssl s_client -connect $SMTP_SERVER:$SMTP_PORT -starttls smtp -crlf <<< "EHLO $SMTP_SERVER" > /dev/null
echo "HELO $SMTP_SERVER" | openssl s_client -starttls smtp -connect $SMTP_SERVER:$SMTP_PORT -crlf -quiet -ign_eof 2>&1 > /dev/null
echo "QUIT" | openssl s_client -starttls smtp -connect $SMTP_SERVER:$SMTP_PORT -crlf -quiet -ign_eof 2>&1 > /dev/null

# Send a test email using telnet
echo "Sending a test email..."
{
  sleep 1
  echo "EHLO $SMTP_SERVER"
  sleep 1
  echo "AUTH LOGIN"
  sleep 1
  echo -n "$SES_USERNAME" | base64
  sleep 1
  echo -n "$SES_PASSWORD" | base64
  sleep 1
  echo "MAIL FROM: <$SENDER_EMAIL>"
  sleep 1
  echo "RCPT TO: <$RECIPIENT_EMAIL>"
  sleep 1
  echo "DATA"
  sleep 1
  echo "Subject: Test Email"
  echo "From: $SENDER_EMAIL"
  echo "To: $RECIPIENT_EMAIL"
  echo "This is a test email."
  echo "."
  sleep 1
  echo "QUIT"
} | openssl s_client -starttls smtp -connect $SMTP_SERVER:$SMTP_PORT -crlf -quiet -ign_eof 2>&1 > /dev/null

echo "Test complete."
