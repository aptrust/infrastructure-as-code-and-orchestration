package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

var (
	wasabiAccessKey = os.Getenv("WASABI_ACCESS_KEY")
	wasabiSecretKey = os.Getenv("WASABI_SECRET_KEY")
	wasabiBucket    = os.Getenv("WASABI_BUCKET")
	wasabiRegion    = os.Getenv("WASABI_REGION")

	awsAccessKey = os.Getenv("AWS_ACCESS_KEY")
	awsSecretKey = os.Getenv("AWS_SECRET_KEY")
	awsBucket    = os.Getenv("AWS_BUCKET")
	awsRegion    = os.Getenv("AWS_REGION")

	runTime = os.Getenv("RUN_TIME") // Expected format: HH:MM (24-hour format)
)

func main() {
	logFile, err := os.OpenFile("/app/logs/app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Failed to open log file: %v", err)
	}
	defer logFile.Close()
	log.SetOutput(logFile)

	for {
		currentTime := time.Now()
		log.Println("Waiting for scheduled run time...")

		// Parse scheduled time
		parsedTime, err := time.Parse("15:04", runTime)
		if err != nil {
			log.Fatalf("Invalid RUN_TIME format, expected HH:MM: %v", err)
		}

		scheduledTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), parsedTime.Hour(), parsedTime.Minute(), 0, 0, currentTime.Location())
		if currentTime.After(scheduledTime) {
			scheduledTime = scheduledTime.Add(24 * time.Hour)
		}

		durationUntilRun := time.Until(scheduledTime)
		log.Printf("Next run scheduled at: %s", scheduledTime)
		time.Sleep(durationUntilRun)

		log.Println("Starting log file sync...")
		err = syncLogs()
		if err != nil {
			log.Printf("Error during log sync: %v", err)
		}
	}
}

func syncLogs() error {
	ctx := context.TODO()

	// Configure Wasabi S3 client
	wasabiCfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(wasabiRegion),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(wasabiAccessKey, wasabiSecretKey, "")),
	)
	if err != nil {
		return fmt.Errorf("failed to configure Wasabi S3 client: %w", err)
	}
	wasabiClient := s3.NewFromConfig(wasabiCfg, func(o *s3.Options) {
		o.BaseEndpoint = "https://s3.wasabisys.com"
	})

	// Configure AWS S3 client
	awsCfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(awsRegion),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(awsAccessKey, awsSecretKey, "")),
	)
	if err != nil {
		return fmt.Errorf("failed to configure AWS S3 client: %w", err)
	}
	awsClient := s3.NewFromConfig(awsCfg)

	// List objects in Wasabi bucket
	objects, err := wasabiClient.ListObjectsV2(ctx, &s3.ListObjectsV2Input{
		Bucket: &wasabiBucket,
	})
	if err != nil {
		return fmt.Errorf("failed to list objects from Wasabi: %w", err)
	}

	// Copy each file from Wasabi to AWS
	for _, obj := range objects.Contents {
		sourceKey := *obj.Key
		copySource := fmt.Sprintf("%s/%s", wasabiBucket, sourceKey)
		_, err := awsClient.CopyObject(ctx, &s3.CopyObjectInput{
			Bucket:     &awsBucket,
			Key:        &sourceKey,
			CopySource: &copySource,
		})
		if err != nil {
			log.Printf("Failed to copy %s: %v", sourceKey, err)
		} else {
			log.Printf("Successfully copied %s", sourceKey)
		}
	}

	return nil
}
